package com.gilmard.javamt._04_basic_synchronization_use_synchronized_for_non_atomic_operations_;

import java.text.DecimalFormat;

/*
 * NON-ATOMIC operations like ++ etc. Synchronized needed.
 */

public class App {

    private int count = 0;

    public static void main(String[] args) {
        App app = new App();

        long startTime = System.nanoTime();
        app.doWork();
        long stopTime = System.nanoTime();
        long duration = stopTime - startTime;

        DecimalFormat df = new DecimalFormat("#0.00");
        System.out.println("time elapsed = " + df.format(duration / 1000000d) + " ms");
    }

    synchronized void increment() {
        count++;
    }

    void doWork() {

        Thread t1 = new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 10000; ++i) {
                    increment();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 10000; ++i) {
                    increment();
                }
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("count = " + count);
    }

}
