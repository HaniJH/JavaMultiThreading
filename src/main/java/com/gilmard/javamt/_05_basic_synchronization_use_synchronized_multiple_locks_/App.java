package com.gilmard.javamt._05_basic_synchronization_use_synchronized_multiple_locks_;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.gilmard.javamt.MyThread;

class Worker {

    private Random random = new Random();

    private Object lock1 = new Object();
    private Object lock2 = new Object();

    private List<Integer> list1 = new ArrayList<Integer>();
    private List<Integer> list2 = new ArrayList<Integer>();

    public void stageOne() {
        synchronized (lock1) {
            list1.add(random.nextInt(1000));
        }
        MyThread.sleepForMs(1);
    }

    public void stageTwo() {
        synchronized (lock2) {
            list2.add(random.nextInt(1000));
        }
        MyThread.sleepForMs(1);
    }

    public void process() {
        for (int i = 0; i < 1000; ++i) {
            stageOne();
            stageTwo();
        }
    }

    public void main(String[] args) {
        long start = System.nanoTime();
        process();
        long end = System.nanoTime();
        System.out.println("duration: " + (end-start)/1000000);
        System.out.println("List1: " + list1.size());
        System.out.println("List2: " + list2.size());
    }

}

public class App {
    public static void main(String[] args) {
        new Worker().main(args);
    }
}
