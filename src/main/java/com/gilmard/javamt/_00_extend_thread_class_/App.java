package com.gilmard.javamt._00_extend_thread_class_;

import com.gilmard.javamt.MyThread;

class Runner extends Thread {

    private final String name;

    public Runner(final String name) {
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println(name + ": iteration " + i);
            MyThread.sleepForMs(200);
        }
    }

}

public class App {

    public static void main(String[] args) {

        System.out.println("App started...");

        Runner runner1 = new Runner("R-ONE");
        runner1.start();

        Runner runner2 = new Runner("R-TWO");
        runner2.start();

        System.out.println("... App ended.");
    }

}
