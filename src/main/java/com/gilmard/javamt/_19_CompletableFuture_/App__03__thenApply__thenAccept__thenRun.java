package com.gilmard.javamt._19_CompletableFuture_;

import com.gilmard.javamt.MyThread;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App__03__thenApply__thenAccept__thenRun {

    public static void main(String[] args) {

        System.out.println("Main started ...");

        ExecutorService executor = Executors.newFixedThreadPool(10);

        CompletableFuture<String> stringCompletableFuture = CompletableFuture
                .supplyAsync(() -> {
                    System.out.println(MyThread.getCurrentThreadName());
                    System.out.println("Thread started ...");
                    int duration = new Random().nextInt(3000);
                    System.out.println("waiting for " + duration + " ms...");
                    MyThread.sleepForMs(duration);
                    System.out.println("Thread finished.");
                    return String.valueOf(duration);
                }, executor);

        CompletableFuture<Void> voidCompletableFuture = stringCompletableFuture
                // chaining of 'apply's to process the result
                .thenApply(d -> {
                    System.out.println(MyThread.getCurrentThreadName());
                    return d + " ms";
                })
                .thenApply(s -> {
                    System.out.println(MyThread.getCurrentThreadName());
                    return "waited for " + s + ".";
                })
                // chaining of 'apply's in separate threads
                .thenApply(d -> {
                    System.out.println(MyThread.getCurrentThreadName());
                    MyThread.sleepForMs(2000);
                    return d;
                })
                .thenApplyAsync(d -> {
                    System.out.println(MyThread.getCurrentThreadName());
                    return "PROCESSED[" + d + "]";
                }, executor)
                // run a callback with access to the future data
                .thenAccept(futureResult -> {
                    System.out.println(MyThread.getCurrentThreadName());
                    System.out.println("run a callback with access to the future data");
                    System.out.println(futureResult);
                })
                // run a callback without any access to the future data
                .thenRun(() -> {
                    System.out.println(MyThread.getCurrentThreadName());
                    System.out.println("finally, run a callback without any access to the future data");
                })
                .thenRunAsync(() -> {
                    System.out.println(MyThread.getCurrentThreadName());
                    System.out.println("finally, run ASYNC a callback without any access to the future data");
                }, executor);


        try {
            voidCompletableFuture.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        executor.shutdown();
        System.out.println("... Main finished.");

    }

}
