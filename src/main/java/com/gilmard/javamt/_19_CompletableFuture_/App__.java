package com.gilmard.javamt._19_CompletableFuture_;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.*;

public class App__ {

    public static void main(String[] args) {

        System.out.println("Main started ...");

        ExecutorService executor = Executors.newFixedThreadPool(10);

        CompletableFuture<Integer> completableFuture = new CompletableFuture<>();

        completableFuture.runAsync(()->{ return; });


        Future<Integer> future = executor.submit(() -> {
            System.out.println("Thread started ...");
            int duration = new Random().nextInt(3000);
            System.out.println("waiting for " + duration + " ms...");
            Thread.sleep(duration);
            if (duration > 2000) {
                throw new IOException(duration + " is too long!");
            }
            System.out.println("Thread finished.");
            return duration;
        });

        executor.shutdown();

        try {
            System.out.println("Duration: " + future.get()); // future.get is blocking
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
            System.out.println("Is Future canceled? " + future.isCancelled());
        }

        System.out.println("... Main finished.");

    }

}
