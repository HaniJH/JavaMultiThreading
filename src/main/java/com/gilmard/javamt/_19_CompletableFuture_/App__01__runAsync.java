package com.gilmard.javamt._19_CompletableFuture_;

import com.gilmard.javamt.MyThread;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.*;

public class App__01__runAsync {

    public static void main(String[] args) {

        System.out.println("Main started ...");

        CompletableFuture<Void> voidCompletableFuture = CompletableFuture.runAsync(() -> {
            System.out.println("Thread started ...");
            int duration = new Random().nextInt(3000);
            System.out.println("waiting for " + duration + " ms...");
            MyThread.sleepForMs(duration);
            System.out.println("Thread finished.");
            return;
        });

        try {
            voidCompletableFuture.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        System.out.println("... Main finished.");

    }

}
