package com.gilmard.javamt._19_CompletableFuture_;

import com.gilmard.javamt.MyThread;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class App__04__combining__ {


    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(10);

        final List<Integer> results = new ArrayList<>();

        IntStream.range(0, 10).forEach(i -> results.add(new Random().nextInt(1000)));

        List<CompletableFuture<Integer>> futures = results.stream()
                .map(i ->
                        CompletableFuture.supplyAsync(() -> {
                            System.out.println(MyThread.getCurrentThreadName() + "waiting for " + i);
                            MyThread.sleepForMs(i);
                            return i;
                        })
                )
                .collect(Collectors.toList());

        Set<Integer> set = new HashSet<>();

        CompletableFuture<Void> voidCompletableFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));

        try {
            voidCompletableFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        Collections.unmodifiableList(new ArrayList<>());

        executor.shutdown();
        System.out.println("... Main finished.");

    }
}


