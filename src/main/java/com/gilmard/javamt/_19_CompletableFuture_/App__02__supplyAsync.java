package com.gilmard.javamt._19_CompletableFuture_;

import com.gilmard.javamt.MyThread;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class App__02__supplyAsync {

    public static void main(String[] args) {

        System.out.println("Main started ...");

        CompletableFuture<Integer> integerCompletableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("Thread started ...");
            int duration = new Random().nextInt(3000);
            System.out.println("waiting for " + duration + " ms...");
            MyThread.sleepForMs(duration);
            System.out.println("Thread finished.");
            return duration;
        });

        try {
            System.out.println("waited for " + integerCompletableFuture.get() + " ms");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        System.out.println("... Main finished.");

    }

}
