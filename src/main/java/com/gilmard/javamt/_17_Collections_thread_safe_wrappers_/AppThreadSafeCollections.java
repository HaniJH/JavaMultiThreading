package com.gilmard.javamt._17_Collections_thread_safe_wrappers_;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.gilmard.javamt.MyThread;

class NonThreadSafeCollection {

    private static final List<Integer> list = new ArrayList<>();

    static void perform() {
        for (int i = 0; i < 100; i++) {
            try {
                list.add(new Random().nextInt());
            } catch (Exception e) {
                e.printStackTrace();
            }
            MyThread.sleepForMs(20);
        }
        System.out.println(MyThread.getCurrentThreadName() + " (Non-thread-safe) : size = " + list.size());
    }
}

class ThreadSafeCollection {

    private static final List<Integer> list = Collections.synchronizedList(new ArrayList<>());

    static void perform() {
        for (int i = 0; i < 100; i++) {
            list.add(new Random().nextInt());
            MyThread.sleepForMs(20);
        }
        System.out.println(MyThread.getCurrentThreadName() + " (Thread-safe) : size = " + list.size());
    }
}

public class AppThreadSafeCollections {

    public static void main(String[] args) {

        System.out.println("Main started...");

        final Thread t1 = new Thread(NonThreadSafeCollection::perform);
        final Thread t2 = new Thread(NonThreadSafeCollection::perform);

        final Thread t3 = new Thread(ThreadSafeCollection::perform);
        final Thread t4 = new Thread(ThreadSafeCollection::perform);

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("... Main finished.");
    }
}