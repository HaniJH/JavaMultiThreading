package com.gilmard.javamt._01_implement_runnable_class_;

import com.gilmard.javamt.MyThread;

class Runner implements Runnable {

    private final String name;

    Runner(final String name) {
        this.name = name;
    }

    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println(name + ": iteration " + i);
            MyThread.sleepForMs(200);
        }
    }
}

public class App {

    public static void main(String[] args) {

        System.out.println("App started...");

        Thread t1 = new Thread(new Runner("R-ONE"));
        Thread t2 = new Thread(new Runner("R-TWO"));

        t1.start();
        t2.start();

        t1.run();

        System.out.println("... App ended.");
    }

}