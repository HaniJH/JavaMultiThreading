package com.gilmard.javamt._18_delgation_of_thread_safety_;

import java.util.Collections;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.gilmard.javamt.MyThread;

public class App {

    private static final SafeContainer safeContainer = new SafeContainer();

    public static void main(String[] args) {

        System.out.println("Main started...");

        final Thread t1 = new Thread(App::run);
        final Thread t2 = new Thread(App::run);

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("... Main finished.");
    }

    private static void run() {
        for (int i = 0; i < 1000; i++) {
            final Integer num = new Random().nextInt();
            safeContainer.add(num.toString(), num);
            MyThread.sleepForMs(2);
        }
        System.out.println(MyThread.getCurrentThreadName() + ": size = " + safeContainer.getMap().size());
    }

    /**
     * Thread-safety is delegated to the ConcurrentMap.
     */
    private static class SafeContainer {
        private final ConcurrentMap<String, Integer> concurrentMap;
        private final Map<String, Integer> safeMap;
        CopyOnWriteArrayList<Integer> copyOnWriteArrayList;

        SafeContainer() {
            concurrentMap = new ConcurrentHashMap<>();
            safeMap = Collections.unmodifiableMap(concurrentMap);
        }

        public void add(String string, Integer integer) {
            concurrentMap.put(string, integer);
        }

        public Map<String, Integer> getMap() {
            return safeMap;
        }
    }
}
