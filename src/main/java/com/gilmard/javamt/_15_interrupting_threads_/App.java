package com.gilmard.javamt._15_interrupting_threads_;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class App {

    public static void main(String[] args) {

        System.out.println("Main started...");

        Thread t1 = new Thread(() -> {
            System.out.println("T-1 started...");
            int a = 1;
            int b = 1;
            System.out.println("T-1: " + a);
            boolean stop = false;
            while (!stop) {
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    stop = true;
                    System.out.println("T-1: interrupted!!!");
                    e.printStackTrace();
                }
                System.out.println("T-1: " + b);
                int t = a;
                a = b;
                b += t;
            }

            System.out.println("... T-1 finished.");
        });

        Thread t2 = new Thread(() -> {
            System.out.println("T-2: started...");
            System.out.println("T-2: waiting for interruption...");
            boolean stop = false;
            while (!stop) {
                stop = Thread.currentThread().isInterrupted();
            }
            System.out.println("T-2: interrupted!!!");
            System.out.println("T-2: ... finished.");
        });

        final ExecutorService executorService = Executors.newCachedThreadPool();

        final Future<Integer> future = executorService.submit(() -> {
            System.out.println("T-3 started...");
            int a = 1;
            boolean stop = false;
            while (!stop) {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    stop = true;
                    e.printStackTrace();
                }
                System.out.println("T-3: " + a);
                a += new Random().nextInt(100);
            }

            System.out.println("... T-3 finished.");
            return a;
        });

        t1.start();
        t2.start();

        executorService.shutdown();

        boolean quit = false;
        while (!quit) {
            final Scanner scanner = new Scanner(System.in);
            System.out.println("Main: which thread to stop: (1), (2), (3), or (0) to quit: ");
            switch (scanner.nextInt()) {
                case 0:
                    quit = true;
                    t1.interrupt();
                    break;
                case 1:
                    t1.interrupt();
                    break;
                case 2:
                    t2.interrupt();
                    break;
                case 3:
                    future.cancel(true);
                    break;
            }
        }

        System.out.println("T-3 (Future): is canceled? " + future.isCancelled());
        System.out.println("T-3 (Future):     is done? " + future.isDone());

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("... Main finished.");
    }
}
