package com.gilmard.javamt._08_producer_and_consumer_._08_producer_consumer__AtomicReference_;

import com.gilmard.javamt.MyThread;

public class App {

    private static Container container = new Container();

    public static void main(String[] args) {

        final Thread t1 = new Thread(container::producer);
        final Thread t2 = new Thread(container::consumer);
        final Thread t3 = new Thread(container::shutDown);

        t1.start();
        t2.start();
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
