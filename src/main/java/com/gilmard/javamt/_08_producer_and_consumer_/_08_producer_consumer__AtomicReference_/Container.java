package com.gilmard.javamt._08_producer_and_consumer_._08_producer_consumer__AtomicReference_;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import com.gilmard.javamt.MyThread;

import net.jcip.annotations.ThreadSafe;

@ThreadSafe
class Container {
    private final AtomicReference<Queue<Integer>> queue = new AtomicReference<>(new LinkedList<>());
    private AtomicBoolean stop = new AtomicBoolean(false);

    void producer() {
        while (!stop.get()) {
            final int rnd = getRnd();
            MyThread.sleepForMs(rnd);
            queue.get().add(rnd);
            System.out.println("Produced > " + rnd + ", size: " + queue.get().size());
        }
    }

    void consumer() {
        while (!stop.get()) {
            MyThread.sleepForMs(getRnd());
            if (queue.get().isEmpty()) {
                System.out.println("!Consumed > Empty!" + ", size: " + queue.get().size());
            } else {
                System.out.println("Consumed < " + queue.get().remove() + ", size: " + queue.get().size());

            }
        }
    }

    void shutDown() {
        MyThread.sleepForMs(4000);
        stop.set(true);
        System.out.println("ShutDown:   GAME OVER!   ");
        System.out.println("ShutDown! size: " + queue.get().size());
    }

    private int getRnd() {
        return 100 + new Random().nextInt(150);
    }

}
