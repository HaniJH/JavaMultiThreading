package com.gilmard.javamt._08_producer_and_consumer_._08_producer_consumer__blocking_queue_;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.gilmard.javamt.MyThread;

// BlockingQueue

public class App {

    private static BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(10);

    private static volatile boolean stop = false;

    public static void main(String[] args) {

        Thread t1 = new Thread(App::producer);
        Thread t2 = new Thread(App::consumer);
        Thread t3 = new Thread(App::stopChecker);

        t1.start();
        t2.start();
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static void producer() {
        while (!stop) {
            try {
                MyThread.sleepForMs(new Random().nextInt(500));
                queue.put(new Random().nextInt(100));
                System.out.println("> produced. queue size(" + queue.size() + ")");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static void consumer() {
        while (!stop) {
            try {
                MyThread.sleepForMs(new Random().nextInt(500));
                queue.take();
                System.out.println("< consumed. queue size(" + queue.size() + ")");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static void stopChecker() {
        while(!stop) {
            MyThread.sleepForMs(100);
            stop = queue.size() >= 5;
        }
        System.out.println("STOP. queue size(" + queue.size() + ")");
    }
}
