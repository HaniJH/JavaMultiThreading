package com.gilmard.javamt._03_basic_synchronization_use_volatile_to_prevent_caching_;

import java.util.Scanner;

import com.gilmard.javamt.MyThread;

/*
 * IMPORTANT: 'volatile' keyword prevents any caching--> thread synchronization.
 */

class Processor extends Thread {

    private volatile boolean stop = false;

    @Override
    public void run() {

        while (!stop) {
            System.out.println("I am running...");
            MyThread.sleepForMs(1000);
        }
    }

    public void shutdown() {
        stop = true;
    }
}

public class App {

    public static void main(String[] args) {
        Processor p = new Processor();
        p.start();

        System.out.println("press enter to stop...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        p.shutdown();

    }

}
