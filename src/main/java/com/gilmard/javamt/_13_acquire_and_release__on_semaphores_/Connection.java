package com.gilmard.javamt._13_acquire_and_release__on_semaphores_;

import java.util.concurrent.Semaphore;

public class Connection {

    private static Connection instance = new Connection();

    private int connections = 0;

    private Semaphore sem = new Semaphore(10, true);

    private Connection() {
    }

    public static Connection getInstance() {
        return instance;
    }

    public void connect() {

        try {
            sem.acquire();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        try {
            doConnect();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            sem.release();
        }
    }

    private void doConnect() throws InterruptedException {
        synchronized (this) {
            connections++;
            System.out.println(Thread.currentThread().getId() + ", Current connections: " + connections);
        }

        Thread.sleep(1000);

        synchronized (this) {
            connections--;
        }
    }
}
