package com.gilmard.javamt._06_executor_service__thread_pool_;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.gilmard.javamt.MyThread;

// ExecutorService

class Processor implements Runnable {

    private final int id;

    Processor(int id) {
        this.id = id;
    }

    public void run() {
        System.out.println("starting: " + id + ", T-ID: " + Thread.currentThread().getId() + ", T-name: "
                + Thread.currentThread().getName());
        MyThread.sleepForMs(2000);
        System.out.println("completed: " + id);
    }
}

public class App {

    public static void main(String[] args) {

        final ExecutorService executorService = Executors.newFixedThreadPool(3);

        for (int i = 0; i < 10; ++i) {
            executorService.submit(new Processor(i));
        }

        executorService.shutdown();

        System.out.println("All task submitted...");

        try {
            executorService.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("All task completed.");
    }

}
