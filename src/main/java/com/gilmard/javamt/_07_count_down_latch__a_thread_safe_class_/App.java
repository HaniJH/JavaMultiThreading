package com.gilmard.javamt._07_count_down_latch__a_thread_safe_class_;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.gilmard.javamt.MyThread;

// CountDownLatch a thread-safe class

class Processor implements Runnable {

    private CountDownLatch latch;

    Processor(CountDownLatch latch) {
        this.latch = latch;
    }

    public void run() {
        System.out.println(MyThread.getCurrentThreadName() + ": started...");
        System.out.println(MyThread.getCurrentThreadName() + ": >> latch: " + latch.getCount());
        MyThread.sleepForMs(1000);
        latch.countDown();
        System.out.println(MyThread.getCurrentThreadName() + ": << latch: " + latch.getCount());
        System.out.println(MyThread.getCurrentThreadName() + ": ... completed.");
    }
}

public class App {

    public static void main(String[] args) {

        System.out.println(MyThread.getCurrentThreadName() + " started...");

        CountDownLatch latch = new CountDownLatch(3);

        ExecutorService executor = Executors.newFixedThreadPool(2);

        for (int i = 0; i < 5; ++i) {
            executor.submit(new Processor(latch));
        }

        executor.shutdown();

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("\n\n" + MyThread.getCurrentThreadName() + " completed.\n\n");
    }

}
