package com.gilmard.javamt._16_ThreadLocal_;

import com.gilmard.javamt.MyThread;

class StaticCounter {

    private static Integer counter = 0;

    static void count() {
        ++counter;
    }

    static Integer get() {
        return counter;
    }
}

class CounterTester {

    static void perform() {
        for (int i = 0; i < 100; i++) {
            StaticCounter.count();
            MyThread.sleepForMs(20);
        }
        System.out.println(MyThread.getCurrentThreadName() + ": count = " + StaticCounter.get());
    }
}

public class AppWithoutLocalThread {

    public static void main(String[] args) {

        System.out.println("Main (without ThreadLocal) started...");

        final Thread t1 = new Thread(CounterTester::perform);

        final Thread t2 = new Thread(CounterTester::perform);

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("... Main finished.");
    }
}