package com.gilmard.javamt._16_ThreadLocal_;

import com.gilmard.javamt.MyThread;

class StaticCounterWithLocalThread {

    private static ThreadLocal<Integer> counter = ThreadLocal.withInitial(() -> 0);

    static void count() {
        counter.set(counter.get() + 1);
    }

    static Integer get() {
        return counter.get();
    }
}

class CounterWithLocalThreadTester {

    static void perform() {
        for (int i = 0; i < 100; i++) {
            StaticCounterWithLocalThread.count();
            MyThread.sleepForMs(20);
        }
        System.out.println(MyThread.getCurrentThreadName() + ": count = " + StaticCounterWithLocalThread.get());
    }
}

public class AppWithLocalThread {

    public static void main(String[] args) {

        System.out.println("Main (with ThreadLocal) started...");

        final Thread t1 = new Thread(CounterWithLocalThreadTester::perform);

        final Thread t2 = new Thread(CounterWithLocalThreadTester::perform);

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("... Main finished.");
    }
}