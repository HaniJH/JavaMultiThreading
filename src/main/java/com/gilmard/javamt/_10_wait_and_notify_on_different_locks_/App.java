package com.gilmard.javamt._10_wait_and_notify_on_different_locks_;

// waiter and notifier locking on different objects.

import java.util.Random;

import com.gilmard.javamt.MyThread;

class Processor {

    private volatile Object producerLock = new Object();
    private volatile Object consumerLock = new Object();

    private volatile boolean stop = false;

    void shutDown() {
        stop = true;
    }

    void produce() throws InterruptedException {

        while (!stop) {
            System.out.println("> producing...");
            MyThread.sleepForMs(new Random().nextInt(3000));
            synchronized (consumerLock) {
                consumerLock.notify();
            }
            System.out.println("> done. waiting to be consumed... ");
            synchronized (producerLock) {
                producerLock.wait();
            }
            System.out.println("> done.\n");
        }
    }

    void consume() throws InterruptedException {
        while (!stop) {
            System.out.println("< consumer waiting for producer...");
            synchronized (consumerLock) {
                consumerLock.wait();
            }
            System.out.println("< produced! consuming...");
            MyThread.sleepForMs(new Random().nextInt(3000));
            System.out.println("< done.");
            synchronized (producerLock) {
                producerLock.notify();
            }
        }
    }
}

public class App {
    public static void main(String[] args) {

        System.out.println("Available virtual cores: " + MyThread.getAvailableProcessors());

        final Processor processor = new Processor();

        Thread t1 = new Thread(() -> {
            try {
                processor.produce();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {
            try {
                processor.consume();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();

        MyThread.sleepForMs(20000);
        processor.shutDown();
        System.out.println("\n\n>>>>   STOP! Game over!   <<<<\n");

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
