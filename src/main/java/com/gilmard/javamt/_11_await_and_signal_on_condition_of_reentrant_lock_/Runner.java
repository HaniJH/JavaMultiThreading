package com.gilmard.javamt._11_await_and_signal_on_condition_of_reentrant_lock_;

import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// re-entrant lock

// CAUTION: Be careful to unlock!
// Condition object

public class Runner {

    private int count = 0;
    private Lock lock = new ReentrantLock();
    private Condition cond = lock.newCondition();

    private void increment() {
        for (int i = 0; i < 10000; ++i) {
            count++;
        }
    }

    public void firstThread() throws InterruptedException {
        lock.lock();
        System.out.println("T-1: waiting...");
        cond.await();
        System.out.println("T-1: ... waked up!");
        try {
            increment();
        } finally {
            lock.unlock();
        }
        System.out.println("T-1: done!");
    }

    public void secondThread() throws InterruptedException {
        Thread.sleep(100);
        lock.lock();
        System.out.println("T-2: press the return key...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        System.out.println("T-2: ... pressed!");
        cond.signal();
        System.out.println("T-2: signal sent");

        try {
            increment();
        } finally {
            lock.unlock();
        }
        System.out.println("T-2: done!");
    }

    public void finished() {
        System.out.println("count: " + count);
    }
}

