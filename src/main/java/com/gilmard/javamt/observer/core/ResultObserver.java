package com.gilmard.javamt.observer.core;


import com.gilmard.javamt.observer.ThreadUtil;

public class ResultObserver implements Observer<RandomResult> {

    private int eventCounter = 0;

    @Override
    public void update(RandomResult randomResult) {
//        new Thread(() -> {
            System.out.println("\nReceiver @ " + ThreadUtil.getCurrentThreadName());
            eventCounter++;
            System.out.println("a new result received (" + eventCounter + "):");
            System.out.println(randomResult.isNegative() ? "  - Negative" : "  - Non-negative");
            System.out.println("  - value: " + randomResult.getResult());
//        }).start();
    }

    @Override
    public void onError(int errorCode) {
        System.out.println("[ ! ] Oh! an error with code of " + errorCode + " is happened :(");
    }
}
