package com.gilmard.javamt.observer.core;

public interface Observable<T> {

    void register(final Observer<T> observer);
}
