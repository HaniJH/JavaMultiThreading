package com.gilmard.javamt.observer.core;

public class RandomResult {

    private final int result;

    public RandomResult(int result) {
        this.result = result;
    }

    public boolean isNegative() {
        return result < 0;
    }

    public int getResult() {
        return result;
    }
}
