package com.gilmard.javamt.observer.core;

public interface Observer<T> {

    void update(final T t);

    void onError(final int errorCode);
}
