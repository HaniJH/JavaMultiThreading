package com.gilmard.javamt.observer.app;

import com.gilmard.javamt.observer.dependent.ResultGenerator;
import com.gilmard.javamt.observer.dependent.ResultGeneratorCompletableFuture;
import com.gilmard.javamt.observer.core.ResultObserver;

public class AppCompletableFuture {

    public static void main(String[] args) {

        ResultObserver resultObserver = new ResultObserver();
        ResultGenerator resultGenerator = new ResultGeneratorCompletableFuture();
        resultGenerator.register(resultObserver);
        resultGenerator.perform();
    }
}
