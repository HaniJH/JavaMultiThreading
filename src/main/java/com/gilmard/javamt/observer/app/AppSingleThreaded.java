package com.gilmard.javamt.observer.app;

import com.gilmard.javamt.observer.dependent.ResultGenerator;
import com.gilmard.javamt.observer.core.ResultObserver;

public class AppSingleThreaded {

    public static void main(String[] args) {

        ResultObserver resultObserver = new ResultObserver();
        ResultGenerator resultGenerator = new ResultGenerator();
        resultGenerator.register(resultObserver);
        resultGenerator.perform();
    }
}
