package com.gilmard.javamt.observer.app;

import com.gilmard.javamt.observer.dependent.ResultGenerator;
import com.gilmard.javamt.observer.dependent.ResultGeneratorIndividualThread;
import com.gilmard.javamt.observer.core.ResultObserver;

public class AppMultiThreaded {

    public static void main(String[] args) {

        ResultObserver resultObserver = new ResultObserver();
        ResultGenerator resultGenerator = new ResultGeneratorIndividualThread();
        resultGenerator.register(resultObserver);
        resultGenerator.perform();
    }
}
