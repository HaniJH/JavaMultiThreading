package com.gilmard.javamt.observer.dependent;

import com.gilmard.javamt.observer.ThreadUtil;
import com.gilmard.javamt.observer.core.Observable;
import com.gilmard.javamt.observer.core.Observer;
import com.gilmard.javamt.observer.core.RandomResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ResultGenerator implements Observable<RandomResult> {

    private final List<Observer<RandomResult>> observers = new ArrayList<>();

    @Override
    public synchronized void register(Observer<RandomResult> observer) {
        observers.add(observer);
        System.out.println("An observer registered.");
    }

    public synchronized void perform() {
        while (true) {
            inform(generate());
        }
    }

    String name() {
        return "Result Generator (single threaded)";
    }

    int generate() {
        System.out.println("\nGenerator @ " + ThreadUtil.getCurrentThreadName());
        int randomNumber = new Random().nextInt(1000);
        ThreadUtil.sleepForMs(1000 + randomNumber);
        System.out.println(name() + ": a random number is created (" + randomNumber + ")");
        return randomNumber;
    }

    synchronized void inform(final int randomNumber) {
        final RandomResult randomResult = new RandomResult(randomNumber);
        observers.forEach(o -> {
            o.update(randomResult);
            if (randomNumber >= 800) {
                o.onError(randomNumber);
            }
        });
    }
}
