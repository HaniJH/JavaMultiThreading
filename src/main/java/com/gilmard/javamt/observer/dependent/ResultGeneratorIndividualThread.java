package com.gilmard.javamt.observer.dependent;

public class ResultGeneratorIndividualThread extends ResultGenerator {

    @Override
    public void perform() {
        new Thread(super::perform).start();
    }

    String name() {
        return "Result Generator (multi threaded)";
    }
}
