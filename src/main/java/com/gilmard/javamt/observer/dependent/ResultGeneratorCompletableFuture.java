package com.gilmard.javamt.observer.dependent;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class ResultGeneratorCompletableFuture extends ResultGenerator {

    String name() {
        return "Result Generator (completable future)";
    }

    @Override
    public void perform() {
        CompletableFuture<Void> voidCompletableFuture = CompletableFuture.runAsync(super::perform);
        try {
            voidCompletableFuture.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
