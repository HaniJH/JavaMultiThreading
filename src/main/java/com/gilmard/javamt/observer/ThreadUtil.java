package com.gilmard.javamt.observer;

/*
NOTE: this is a temporary class for illustration purposes and that's why it contains static methods.
 */
public final class ThreadUtil {

    private ThreadUtil() {
        // to prevent instantiation
    }

    public static void sleepForMs(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String getCurrentThreadName() {
        return  Thread.currentThread().getName();
    }
}
