package com.gilmard.javamt;

public final class MyThread {

    private MyThread() {
        // to prevent instantiation
    }

    public static void sleepForMs(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String getCurrentThreadName() {
        return  Thread.currentThread().getName();
    }

    public static int getAvailableProcessors() {
        return Runtime.getRuntime().availableProcessors();
    }
}
