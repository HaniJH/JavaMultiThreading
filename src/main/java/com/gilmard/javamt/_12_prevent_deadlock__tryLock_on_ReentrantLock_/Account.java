package com.gilmard.javamt._12_prevent_deadlock__tryLock_on_ReentrantLock_;

class Account {

    private int balance;

    Account(int balance) {
        this.balance = balance;
    }

    void deposite(int amount) {
        balance += amount;
    }

    void withdraw(int amount) {
        balance -= amount;
    }

    int getBalance() {
        return balance;
    }

    static void transfer(Account src, Account dst, int amount) {
        src.withdraw(amount);
        dst.deposite(amount);
    }
}
