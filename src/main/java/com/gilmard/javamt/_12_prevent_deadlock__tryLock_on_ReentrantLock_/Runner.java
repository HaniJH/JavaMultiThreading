package com.gilmard.javamt._12_prevent_deadlock__tryLock_on_ReentrantLock_;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// deadlock

class Runner {

    private Account acc1 = new Account(10000);
    private Account acc2 = new Account(10000);

    private Lock lock1 = new ReentrantLock();
    private Lock lock2 = new ReentrantLock();

    private void acquireLocks() throws InterruptedException {

        boolean done = false;

        while (!done) {
            boolean gotLock1 = false;
            boolean gotLock2 = false;

            try {
                gotLock1 = lock1.tryLock();
                gotLock2 = lock2.tryLock();
                done = gotLock1 && gotLock2;
            } finally {
                if (!done) {
                    if (gotLock1) {
                        lock1.unlock();
                    }
                    if (gotLock2) {
                        lock2.unlock();
                    }
                }
            }
        }
    }

    private void releaseLocks(){
        lock1.unlock();
        lock2.unlock();
    }

    void firstThread() throws InterruptedException {
        acquireLocks();
        try {
            Account.transfer(acc1, acc2, new Random().nextInt(100));
        } finally {
            releaseLocks();
        }
    }

    void secondThread() throws InterruptedException {
        acquireLocks();
        try {
            Account.transfer(acc1, acc2, new Random().nextInt(100));
        } finally {
            releaseLocks();
        }
    }

    public void finished() {
        System.out.println("\n    account 1 : " + acc1.getBalance());
        System.out.println("    account 2 : " + acc2.getBalance());
        System.out.println("total balance : " + (acc1.getBalance() + acc2.getBalance()));
        System.out.println("\nfinished.");
    }
}
