package com.gilmard.javamt._02_implement_runnable_on_the_fly_;

import com.gilmard.javamt.MyThread;

public class App {

    static void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("* 3 * : iteration " + i);
            MyThread.sleepForMs(200);
        }
    }


    public static void main(String[] args) {

        System.out.println("Main started...");
        Thread t1 = new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 5; i++) {
                    System.out.println("* 1 * : iteration " + i);
                    MyThread.sleepForMs(200);
                }
            }
        });
        t1.start();

        // with Lambda
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("_ 2 _: iteration " + i);
                MyThread.sleepForMs(200);
            }
        });
        t2.start();

        // with Lambda with method reference
        Thread t3 = new Thread(App::run);
        t3.start();


        System.out.println("...Main finished.");

    }
}
