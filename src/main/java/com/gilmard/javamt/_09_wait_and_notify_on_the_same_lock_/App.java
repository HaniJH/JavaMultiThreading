package com.gilmard.javamt._09_wait_and_notify_on_the_same_lock_;

// waiter and notifier must be locked on the same object.

import java.util.Random;

import com.gilmard.javamt.MyThread;

class Processor {

    private volatile Object lock = new Object();

    private volatile boolean stop = false;

    public void shutDown() {
        stop = true;
    }

    public void produce() throws InterruptedException {

        while (!stop) {
            System.out.println("> producing...");
            MyThread.sleepForMs(new Random().nextInt(3000));
            synchronized (lock) {
                lock.notify();
            }
            System.out.println("> done. waiting to be consumed... ");
            synchronized (lock) {
                lock.wait();
            }
            System.out.println("> done.\n");
        }
    }

    public void consume() throws InterruptedException {
        while (!stop) {
            System.out.println("< consumer waiting for producer...");
            synchronized (lock) {
                lock.wait();
            }
            System.out.println("< produced! consuming...");
            MyThread.sleepForMs(new Random().nextInt(3000));
            System.out.println("< done.");
            synchronized (lock) {
                lock.notify();
            }
        }
    }
}

public class App {
    public static void main(String[] args) {

        System.out.println("Available virtual cores: " + MyThread.getAvailableProcessors());

        final Processor processor = new Processor();

        Thread t1 = new Thread(() -> {
            try {
                processor.produce();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread t2 = new Thread(() -> {
            try {
                processor.consume();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();

        MyThread.sleepForMs(20000);
        processor.shutDown();
        System.out.println("\n\n>>>> STOP <<<<\n");

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
